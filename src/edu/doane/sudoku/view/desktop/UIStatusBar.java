package edu.doane.sudoku.view.desktop;

import java.awt.FlowLayout;
import java.awt.Graphics;
import java.awt.GridLayout;
import javax.swing.JLabel;
import javax.swing.JPanel;

/**
 * Status bar for the desktop SuDoKu game.
 *
 * @author Mark M. Meysenburg
 * @version 12/16/2015
 */
public class UIStatusBar extends JPanel {
	/**
	 * Silence the Eclipse warning about serialization, even though
	 * we're not doing that. 
	 */
	private static final long serialVersionUID = 1L;

    /**
     * Label displaying the elapsed time.
     */
    private final JLabel lblTimer;

    /**
     * Label displaying notes mode status.
     */
    private final JLabel lblNotesMode;

    /**
     * Construct the status bar.
     */
    public UIStatusBar() {
        super();

        setLayout(new GridLayout(1, 2));

        lblTimer = new JLabel("0:00:00");

        JPanel pnlTimer = new JPanel();
        pnlTimer.setLayout(new FlowLayout());
        pnlTimer.add(lblTimer);
        add(pnlTimer);

        lblNotesMode = new JLabel();
        setNormalMode();
        JPanel pnlNotesMode = new JPanel();
        pnlNotesMode.setLayout(new FlowLayout());
        pnlNotesMode.add(lblNotesMode);
        add(pnlNotesMode);
    }

    /**
     * Toggle notes mode on.
     */
    public final void setNotesMode() {
        lblNotesMode.setText("(N)otes mode: on");
    }

    /**
     * Toggle notes mode off.
     */
    public final void setNormalMode() {
        lblNotesMode.setText("(N)otes mode: off");
    }

    @Override
    public void paint(Graphics g) {
        super.paint(g);
    }
    
    /**
     * Set the time value displayed on the status bar.
     * 
     * @param time Time value to set, in 0:00:00 format.
     */
    public void setTime(String time) {
        lblTimer.setText(time);
    }
}
